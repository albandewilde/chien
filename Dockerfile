FROM golang as builder

WORKDIR /usr/src/chien
COPY . .

RUN CGO_ENABLED=0 go build -o /bin/chien

FROM alpine

COPY --from=builder /bin/chien .

CMD ["./chien"]
