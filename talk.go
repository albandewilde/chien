package main

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"os"
)

func talk(msg, hookURL string) error {
	payload, err := json.Marshal(
		map[string]string{
			"content":  msg,
			"username": os.Getenv("dog_name"),
		},
	)

	if err != nil {
		return err
	}

	_, err = http.Post(hookURL, "application/json", bytes.NewReader(payload))

	return err
}

type talking func() (string, error)

func woofJardin() (string, error) {
	// Choose message content
	// - "Woof !" (0.75)
	// - "💩" (0.25)

	var msg string
	if rand.Intn(4) == 0 {
		msg = "💩"
	} else {
		msg = "Woof !"
	}

	return msg, talk(msg, os.Getenv("hook_url_jardin"))
}

func woofCuisine() (string, error) {
	msg := "Ouaf"
	return msg, talk(msg, os.Getenv("hook_url_cuisine"))
}
