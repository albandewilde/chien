package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

func main() {
	for {
		// Choose random duration to wait (between 6 hours and 3 days)
		d, err := randomDuration(time.Hour*6, time.Hour*72)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Printf("%s %s\n", "Waiting ", d.String())

		// Wait a little
		<-time.After(d)

		// Choose a function to talk
		talkFuncs := []talking{
			woofJardin,
			woofCuisine,
		}
		talkFunc := talkFuncs[rand.Intn(len(talkFuncs))]

		// Talk
		msg, err := talkFunc()
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf(
				"%s: %s\n",
				time.Now().Format("2006/02/01 15:04:05"),
				msg,
			)
		}
	}
}

func randomDuration(min, max time.Duration) (time.Duration, error) {
	gap := (max - min).Nanoseconds()

	// Calculate the random duration
	d, err := time.ParseDuration(strconv.FormatInt(rand.Int63n(gap), 10) + "ns")

	return d + min, err
}
